<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomepageController; 
use App\Http\Controllers\AdminController; 
use App\Http\Controllers\TarianController; 

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomepageController::class, 'index'])->name('root');
Route::get('/detail/{id}', [HomepageController::class, 'detail'])->name('detail');

Route::view('/tentang', '_partials.tentang')->name('desc');

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

Route::prefix('admin')->group(function() {

    Route::get('/dashboard', [AdminController::class, 'index'])
        ->name('admin.dashboard')
        ->middleware('auth');
    
    Route::get('/tarian', [TarianController::class, 'index'])
        ->name('tarian')
        ->middleware('auth');
    
    Route::get('/tarian/tambah', [TarianController::class, 'create'])
        ->name('tarian.create')
        ->middleware('auth');
    
    Route::post('/tarian/tambah', [TarianController::class, 'store'])
        ->name('tarian.store')
        ->middleware('auth');
    
    Route::post('/tarian/upload-gambar', [TarianController::class, 'ImageUpload'])
        ->name('tarian.upload.gambar')
        ->middleware('auth');

    Route::get('tarian/{id}/edit', [TarianController:: class, 'edit'])
        ->name('tarian.edit')
        ->middleware('auth');
    
    Route::put('tarian/{id}', [TarianController:: class, 'update'])
        ->name('tarian.update')
        ->middleware('auth');
        
    Route::delete('/tarian/{id}', [TarianController::class, 'destroy'])
        ->name('tarian.delete')
        ->middleware('auth');



});

require __DIR__.'/auth.php';
