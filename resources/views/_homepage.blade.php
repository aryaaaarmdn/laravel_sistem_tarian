@extends('_app')

@section('content')
    <h1 class="text-center">List Informasi Tarian</h1>
    <hr>
    @if( count($data) === 0 )
        <h3 class="text-info">Maaf, Belum Ada Data</h3>
    @else
        <!-- <div class="card-group"> -->
        <div class="row row-cols-1 row-cols-lg-4 g-4">  
            @foreach($data as $tarian)
                <div class="col">
                    <div class="card m-2">
                        <div class="card-header">
                            {{ $tarian->nama }} - {{ $tarian->daerah_asal }}
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">Deskripsi</h6>
                            <p class="card-text">
                                {{ Str::limit($tarian->deskripsi, 50) }}
                            </p>
                            <a href="{{ route('detail', $tarian->id) }}" 
                                class="btn btn-primary btn-sm">
                                Detail
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
@endsection