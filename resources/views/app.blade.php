@include('_layouts.header')

    <div class="gtco-loader"></div>

    <div id="page">
		<nav class="gtco-nav" role="navigation">
			<div class="container">
				<div class="row">
					<div class="col-xs-2 text-left">
						<div id="gtco-logo"><a href="{{ route('root') }}">Verb<span>.</span></a></div>
					</div>
					<div class="col-xs-10 text-right menu-1">
						<ul>
							<li>
								<a href="{{ route('desc') }}">Tentang</a>
							</li>
							<li>
								<a href="{{ route('login') }}">Login</a>
							</li>
						</ul>
					</div>
				</div>

			</div>
		</nav>

		<header id="gtco-header" class="gtco-cover" role="banner" 
			data-stellar-background-ratio="0.5" style="background-color:#3B3F43">
			<!-- <div class="overlay"></div> -->
			@yield('overlay')
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-left">
						<div class="display-t">
							<div class="display-tc animate-box" data-animate-effect="fadeInUp">
								<h1 class="mb30 text-center">
                                    @if( Route::currentRouteName() == 'detail' )
                                        {{ $data->nama }} - {{ $data->daerah_asal }}
                                    @elseif( Route::currentRouteName() == 'root' )
                                        Seputar Informasi Tarian Indonesia
									@else
										Tentang
                                    @endif
                                </h1>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>

        @yield('content')

		<footer id="gtco-footer" role="contentinfo">
			<div class="container">
				<div class="row copyright">
					<div class="col-md-12 text-center">
						<p>
							<small class="block">&copy; 2016 Free HTML5. All Rights Reserved.</small>
							<small class="block">Designed by <a href="http://gettemplates.co/"
									target="_blank">GetTemplates.co</a> Demo Images: <a href="http://unsplash.com/"
									target="_blank">Unsplash</a></small>
						</p>
						<p>
						<ul class="gtco-social-icons">
							<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-linkedin"></i></a></li>
							<li><a href="#"><i class="icon-dribbble"></i></a></li>
						</ul>
						</p>
					</div>
				</div>

			</div>
		</footer>
	</div>

    <div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>

@include('_layouts.footer')