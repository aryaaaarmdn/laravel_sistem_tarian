@extends('_app')

@section('content')
    <div class="card">
        @if( !isset($data->img) )
            <svg class="bd-placeholder-img card-img-top" width="100%" height="180" x
                mlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Image cap" preserveAspectRatio="xMidYMid slice" focusable="false">
                <title>Placeholder</title>
                <rect width="100%" height="100%" fill="#868e96"></rect>
                <text x="50%" y="50%" fill="#dee2e6" dy=".3em">Image cap</text>
            </svg>
        @else 
            <img src="#" class="card-img-top" alt="Foto">
        @endif
        <div class="card-body">
            <h5 class="card-title">{{ $data->nama }}</h5>
            <h6 class="card-subtitle mb-2 text-muted">{{ $data->daerah_asal }}</h6>
            <p class="card-text mt-4">{{ $data->deskripsi }}</p>
        </div>
    </div>
@endsection
