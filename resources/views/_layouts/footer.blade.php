<!-- jQuery -->
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
	<!-- jQuery Easing -->
	<script src="{{ asset('assets/js/jquery.easing.1.3.js') }}"></script>
	<!-- Bootstrap -->
	<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
	<!-- Waypoints -->
	<script src="{{ asset('assets/js/jquery.waypoints.min.js') }}"></script>
	<!-- Stellar -->
	<script src="{{ asset('assets/js/jquery.stellar.min.js') }}"></script>
	<!-- Main -->
	<script src="{{ asset('assets/js/main.js') }}"></script>

</body>

</html>