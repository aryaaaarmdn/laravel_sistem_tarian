@extends('app')

@section('overlay')
	<div class="overlay" 
		style="background-image: url({{ asset('upload/gambar/tarian_thumbnail') . '/' . $data->img_thumbnail }});
				background-repeat: no-repeat;
                background-size: 100% 100%
	">
	</div>
@endsection

@section('content')

    <div id="gtco-maine">
		<div class="container">
			<div class="row row-pb-md">
				<div class="col-md-12">
					<article class="mt-negative">
						@if(!$data->img)
							<div class="text-left content-article">
								<div class="row">
									<div class="col-lg-12 cp-r animate-box">
										<p>{!! $data->deskripsi !!}</p>
									</div>
								</div>
							</div>
						@else
							<div class="text-left content-article">
								<div class="row row-pb-sm">
									<div class="col-lg-6 col-md-12 animate-box">
										<figure>
											<img src="{{ asset('upload/tarian') . '/' . $data->img }}"
												alt=""
												class="img-responsive">
											<figcaption>
												{{ $data->nama }} - {{ $data->daerah_asal }}
											</figcaption>
										</figure>
									</div>
									<div class="col-lg-6 col-md-12 cp-l animate-box">
										{!! $data->deskripsi !!}
									</div>
								</div>
							</div>
						@endif
					</article>
				</div>
			</div>
		</div>
	</div>

@endsection