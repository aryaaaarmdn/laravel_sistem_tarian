@extends('app')

@section('overlay')
	<div class="overlay" 
        style="background-image: url({{ asset('assets/images/tariandefault.jpg') }}); 
                background-repeat: no-repeat;
                background-size: 100% 100%;
    ">
    </div>
@endsection

@section('content')

<div id="gtco-main">
    <div class="container">
        <div class="row row-pb-md">
            <div class="col-md-12">
                <ul id="gtco-post-list">
                @foreach($data as $tarian)
                        @if($loop->first || $loop->last)
                            <li class="full entry animate-box" data-animate-effect="fadeIn">
                                <a href="{{ route('detail', $tarian->id) }}">
                                    @if( !$tarian->img_thumbnail )
                                        <div class="entry-img" style="background-image: url({{ asset('assets/images/img_5.jpg') }})"></div>
                                    @else
                                        <div class="entry-img" style="background-image: url({{ asset('upload/gambar/tarian_thumbnail/') . '/' . $tarian->img_thumbnail }})"></div>
                                    @endif

                                    <div class="entry-desc">
                                        <h3>{{ $tarian->nama }} - {{ $tarian->daerah_asal }}</h3>
                                        <p>{!! Str::limit($tarian->deskripsi, 300) !!}</p>
                                    </div>
                                </a>
                                <a href="{{ route('detail', $tarian->id) }}" class="post-meta">
                                    <span class="date-posted">Baca Selengkapnya</span>
                                </a>
                            </li>
                        @elseif($loop->odd)
                            <li class="one-third entry animate-box" data-animate-effect="fadeIn">
                                <a href="{{ route('detail', $tarian->id) }}">
                                    @if( !$tarian->img_thumbnail )
                                        <div class="entry-img" style="background-image: url({{ asset('assets/images/img_5.jpg') }})"></div>
                                    @else
                                        <div class="entry-img" style="background-image: url({{ asset('upload/gambar/tarian_thumbnail/') . '/' . $tarian->img_thumbnail }})"></div>
                                    @endif

                                    <div class="entry-desc">
                                        <h3>{{ $tarian->nama }} - {{ $tarian->daerah_asal }}</h3>
                                        <p>{!! Str::limit($tarian->deskripsi, 300) !!}.</p>
                                    </div>
                                </a>
                                <a href="{{ route('detail', $tarian->id) }}" class="post-meta"> 
                                    <span class="date-posted">Baca Selengkapnya</span>
                                </a>
                            </li>
                        @elseif($loop->even)
                            <li class="two-third entry animate-box" data-animate-effect="fadeIn">
                                <a href="{{ route('detail', $tarian->id) }}">
                                    @if( !$tarian->img_thumbnail )
                                        <div class="entry-img" style="background-image: url({{ asset('assets/images/img_5.jpg') }})"></div>
                                    @else
                                        <div class="entry-img" style="background-image: url({{ asset('upload/gambar/tarian_thumbnail/') . '/' . $tarian->img_thumbnail }})"></div>
                                    @endif

                                    <div class="entry-desc">
                                        <h3>{{ $tarian->nama }} - {{ $tarian->daerah_asal }}</h3>
                                        <p>{!! Str::limit($tarian->deskripsi, 300) !!}</p>
                                    </div>
                                </a>
                                <a href="{{ route('detail', $tarian->id) }}" class="post-meta">
                                    <span class="date-posted">Baca Selengkapnya</span>
                                </a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
		</div>

@endsection
