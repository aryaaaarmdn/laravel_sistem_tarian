@extends('admin_app')

@section('title')
    <h1 class="mt-4">Data Tarian</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb mb-4 mt-4">
        <li class="breadcrumb-item active">Tarian</li>
    </ol>
@endsection

@section('content')

    @if(session('pesan'))
        <div class="alert alert-success">{{ session('pesan') }}</div>
    @endif

    <div class="table-responsive">
        <a href="{{ route('tarian.create') }}" 
            class="btn btn-primary btn-sm mb-3">
            Tambah Data Baru
        </a>
        <!-- <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0"> -->
        <table class="table table-bordered" id="" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>Daerah Asal</th>
                    <th width="35%">Deskripsi</th>
                    <th>Image</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $tarian)
                    <tr>
                        <td>{{ $tarian->nama }}</td>
                        <td>{{ $tarian->daerah_asal }}</td>
                        <td>{!! $tarian->deskripsi !!}</td>
                        <td>
                            @if(!$tarian->img_thumbnail)
                                <span><i>Belum Ada Gambar Default</i></span>
                            @else
                                <img src="{{ asset('upload/gambar/tarian_thumbnail') . '/' . $tarian->img_thumbnail }}" alt="" class="img-thumbnail">
                            @endif
                        </td>
                        <td width="10%">
                            <a href="{{ route('tarian.edit', ['id' => $tarian->id]) }}" class="btn btn-sm btn-info">Edit</a>
                            <form action="{{ route('tarian.delete', $tarian->id) }}" 
                                method="post"
                                style="display: inline">
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn btn-sm btn-danger"
                                    onclick="return confirm('Yakin Ingin Dihapus?');">
                                    Hapus
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection