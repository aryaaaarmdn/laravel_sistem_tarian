@extends('admin_app')

@section('title')
    <h1 class="mt-4">Dashboard</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb mb-4 mt-4">
        <li class="breadcrumb-item active">Dashboard</li>
    </ol>
@endsection

@section('content')
    <div class="col-xl-3 col-md-6">
        <div class="card bg-primary text-white mb-4">
            <div class="card-body">Total Data Tarian</div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <small class="small text-white">{{ count($data) }} Tarian</small>
            </div>
        </div>
    </div>
@endsection