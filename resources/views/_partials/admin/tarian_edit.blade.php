@extends('admin_app')

@section('title')
    <h1 class="mt-4">Edit Data Tarian</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb mb-4 mt-4">
        <li class="breadcrumb-item">
            <a href="{{ route('tarian') }}">Tarian</a>
        </li>
        <li class="breadcrumb-item active">Edit Data Tarian</li>
    </ol>
@endsection

@section('content')
    @if(session('pesan'))
        <div class="alert alert-danger">{{ session('pesan') }}</div>
    @endif

    <div class="row">
        <div class="col-lg-7">
            <form method="POST" action="{{ route('tarian.update', ['id' => $data->id]) }}" 
                enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="small mb-1" for="nama">Nama Tarian</label>
                            <input class="form-control py-4" id="nama" type="text" placeholder="Masukkan Nama Tarian" name="nama" 
                            value="{{ old('nama', $data->nama) }}"/>
                            @error('nama')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="small mb-1" for="asal_daerah">Asal Daerah</label>
                            <input class="form-control py-4" id="asal_daerah" type="text" placeholder="Masukkan Daerah Asal Tarian" name="asal_daerah"
                            value="{{ old('asal_daerah', $data->daerah_asal) }}"/>
                            @error('asal_daerah')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="small mb-1" for="desc">Deskripsi</label>
                    <textarea name="deskripsi" class="form-control"
                        id="desc"
                        placeholder="Masukkan Informasi Deskripsi Tentang Tarian"
                        style="display: block; width: 100%">
                        {{ $data->deskripsi }}
                    </textarea>
                    @error('deskripsi')
                        <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="small mb-1" for="img">Gambar</label>
                    <input name="img" class="form-control py-4" id="img" 
                        type="file" style="height:auto"/>
                    @error('img')
                        <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group mt-4 mb-0">
                    <button type="submit" class="btn btn-primary btn-block">
                        Simpan
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection

<!-- @section('ckeditor')
    <script type="text/javascript">
        CKEDITOR.replace( 'desc', {
            filebrowserUploadUrl: "{{ route('tarian.upload.gambar', ['_token' => csrf_token()]) }}",
            filebrowserUploadMethod: 'form',
            shiftEnterMode: CKEDITOR.ENTER_DIV
        });
    </script>
@endsection -->