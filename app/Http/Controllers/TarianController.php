<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tarian;

class TarianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('_partials.admin.tarian', ['data' => Tarian::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('_partials.admin.tarian_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     private function saving(Request $request, $id = null)
     {
        $request->validate([
            'nama' => 'required|regex:/^[a-zA-Z\s]*$/',
            'asal_daerah' => 'required|regex:/^[a-zA-Z\s]*$/',
            'deskripsi' => 'required|string',
            'img' => 'nullable|file|mimes:jpg,jpeg,png',
        ]);
        $tarianModelForUpdate = Tarian::find($id);
        $tarian = Tarian::updateOrCreate(
            ['id' => $id],
            [
                'nama' => $request->nama, 
                'daerah_asal' => $request->asal_daerah,
                'deskripsi' => $request->deskripsi,
                // 'img_thumbnail' => $request->hasFile('img') ? 
                //                         $request->file('img')->hashName() : null
                'img_thumbnail'=> $request->hasFile('img') ? 
                                    $request->file('img')->hashName() :
                                    ($tarianModelForUpdate->img_thumbnail ?
                                        $tarianModelForUpdate->img_thumbnail :
                                        null),
            ]
        );

        // dd($tarian);

        // if ( $request->hasFile('img') ) {
        //     if ( $tarian->img_thumbnail === null ) {
        //         $request->file('img')
        //                 ->move(public_path('upload/gambar/tarian_thumbnail'), $request->file('img')->hashName());
        //     } else { 
        //         unlink(public_path('upload/gambar/tarian_thumbnail') . $tarian->img_thumbnail);
        //         $request->file('img')
        //                 ->move(public_path('upload/gambar/tarian_thumbnail'), $request->file('img')->hashName());
        //     }
        // }
        // dd($tarian->img_thumbnail);

        if ( !$id ) {
            $request->file('img')
                        ->move(public_path('upload/gambar/tarian_thumbnail'), $request->file('img')->hashName());
        } else {
            if ( $request->hasFile('img') ) {
                if ( $tarianModelForUpdate->img_thumbnail === null ) {
                    $request->file('img')
                            ->move(public_path('upload/gambar/tarian_thumbnail'), $request->file('img')->hashName());
                } else { 
                    unlink(public_path('upload/gambar/tarian_thumbnail/') . $tarianModelForUpdate->img_thumbnail);
                    $request->file('img')
                            ->move(public_path('upload/gambar/tarian_thumbnail'), $request->file('img')->hashName());
                }
            }
        }
        
     }

    public function store(Request $request)
    {

        $this->saving($request);

        return redirect()->route('tarian')
                ->with('pesan', 'Berhasil Menambahkan Data Tarian Baru');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Tarian $id)
    { 
        return view('_partials.admin.tarian_edit', ['data' => $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->saving($request, $id);

        return redirect()->route('tarian')
                ->with('pesan', 'Berhasil Edit Data Tarian');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tarian $id)
    {
        $htmlDOM = new \DOMDocument();
        $htmlDOM->loadHTML($id->deskripsi);
        $images = $htmlDOM->getElementsByTagName('img');

        if( !count($images) < 1 ) {
            foreach($images as $image) {
                $sumberGambar = $image->getAttribute('src');
                $gambar = explode('/', $sumberGambar);  
                $namaGambar = end($gambar);
                unlink(public_path('upload/gambar/tarian/') . $namaGambar);
            }
        }

        if ( $id->img_thumbnail ) {
            if(file_exists(public_path('upload/gambar/tarian_thumbnail/') . $id->img_thumbnail)) {
                unlink(public_path('upload/gambar/tarian_thumbnail/') . $id->img_thumbnail);
            }
        }

        $id->delete();

        return redirect()->route('tarian')
            ->with('pesan', 'Data Tarian Berhasil Dihapus');
    }

    public function ImageUpload(Request $request)
    {
        if($request->hasFile('upload')) {

            $fileName = $request->file('upload')->hashName();
            // echo $fileName;
        
            $request->file('upload')->move(public_path('upload/gambar/tarian'), $fileName);
   
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('upload/gambar/tarian/'.$fileName); 
            $msg = 'Image uploaded successfully'; 
            $response = "<script>window.parent.CKEDITOR.tools.callFunction('$CKEditorFuncNum', '$url', '$msg')</script>";

            echo $response;

        }
    }

}
