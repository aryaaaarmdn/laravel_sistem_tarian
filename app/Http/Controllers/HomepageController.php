<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tarian;

class HomepageController extends Controller
{
    public function index() 
    {
        $data = Tarian::all();
        return view('_partials.homepage', ['data' => $data]);
    }

    public function detail(Tarian $id)
    {
        return view('_partials.detail', ['data' => $id]);
    }
}
