<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tarian;

class AdminController extends Controller
{
    public function index()
    {
        $data = Tarian::all();
        return view('_partials.admin.dashboard', ['data' => $data]);
    }
}
