<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tarian extends Model
{
    use HasFactory;

    protected $table = 'tarian';
    protected $fillable = ['nama', 'daerah_asal', 'deskripsi', 'img_thumbnail'];

}
