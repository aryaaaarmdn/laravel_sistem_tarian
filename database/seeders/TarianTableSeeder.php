<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

use App\Models\Tarian;

class TarianTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $desc = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae repellat quis modi dignissimos eos placeat assumenda, recusandae ea eaque dolor in illo ab. In, optio. Enim voluptas nesciunt autem ipsam, porro totam fugit quo animi harum voluptates excepturi placeat possimus eos id sit voluptatum fuga necessitatibus esse voluptate distinctio nobis omnis inventore. Itaque repellendus voluptatibus minus similique minima molestiae ad, accusamus at quisquam inventore consequatur, ipsa, harum officiis porro vitae consectetur? Veritatis, iste. Corporis modi illo, ullam quas dolores voluptatibus ad adipisci, cumque neque eos accusamus ducimus beatae! Harum delectus atque sequi! Rem id exercitationem blanditiis laudantium, impedit aliquam, atque dolor itaque repellat, natus ducimus laborum voluptas! Nihil tempore officia illo voluptatem cumque? Sint nesciunt commodi quia, fuga eius totam dignissimos ipsum voluptate libero quae autem cupiditate aut, sunt optio architecto alias maiores ut. Rerum recusandae exercitationem commodi officiis a. Officiis odit dolores nisi ex necessitatibus fuga, quam fugiat vero voluptatum veniam nemo, at aperiam officia? Quam, vero. Harum ipsum cupiditate quidem voluptas quam obcaecati quasi sed dolorem dolore, neque porro nulla, nihil eius veritatis error. At ad perspiciatis, incidunt facilis ipsam atque voluptatem mollitia soluta laborum eum recusandae consectetur, pariatur assumenda aliquid. Illo aut non optio architecto, mollitia repellendus vel pariatur fugiat? Aut iusto nostrum quia est deleniti libero amet, perspiciatis provident nisi pariatur repudiandae similique temporibus recusandae mollitia fugiat accusamus eligendi expedita eius sed blanditiis quae modi asperiores.';

        $data = [
            [
                'nama' => 'Tari Kecak',
                'daerah_asal' => 'Bali',
                'deskripsi' => $desc,
            ],
            [
                'nama' => 'Tari Jaipong',
                'daerah_asal' => 'Jawa Barat',
                'deskripsi' => $desc,
            ],
            [
                'nama' => 'Tari Piring',
                'daerah_asal' => 'Sumatera Barat',
                'deskripsi' => $desc,
            ],
            [
                'nama' => 'Tari Serimpi',
                'daerah_asal' => 'Jawa Tengah',
                'deskripsi' => $desc,
            ],
            [
                'nama' => 'Tari Piso Surit',
                'daerah_asal' => 'Sumatera Utara',
                'deskripsi' => $desc,
            ],
        ];

        foreach ($data as $data) {
            Tarian::create($data);
        }

    }
}
