<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTarianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tarian', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('daerah_asal');
            $table->text('deskripsi')->nullable();
            $table->string('img_thumbnail')->nullable();
            $table->timestamps();
        });

        Artisan::call('db:seed', [
            '--class' => TarianTableSeeder::class
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tarian');
    }
}
